import Vue from 'vue'
import VueFormulate from '@braid/vue-formulate'
import MyFormulateAutocomplete from '../components/customComponent/MyFormulateAutocomplete'
import SearchSelect from '../components/customComponent/SearchSelect'
import ElementDatePicker from '../components/customComponent/ElementDatePicker'



Vue.component('MyFormulateAutocomplete',MyFormulateAutocomplete)
Vue.component('SearchSelect',SearchSelect)
Vue.component('ElementDatePicker',ElementDatePicker)

Vue.use(VueFormulate,{
    library:{
        autocomplete:{
            classification:"text",
            component:"MyFormulateAutocomplete"
        },
        selectsearch:{
            classification:"select",
            component:SearchSelect
        },
        elementdatepicker:{
            classification:"date",
            component:ElementDatePicker
        }
    }
});