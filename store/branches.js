/* eslint-disable no-console */
import Vue from 'vue'
export const state = () => ({
	branchList: [
        {
            id:1,
            name:"Dharwad"
        },
        {
            id:2,
            name:"Bangalore"
        }
    ]
})

export const mutations = {
	setBranchList(state, list) {
		state.branchList = list
	}
}

export const getters = {
	getBranchList(state) {
		return state.branchList
	}
}

export const actions = {
	
}
