/* eslint-disable no-console */
import Vue from 'vue'
export const state = () => ({
    deployedAt:[
        {
            id:1,
            name:"Cli9 - TOPLINE BUSINESS SERVICES PVT LTD - BENGALORE"
        },
        {
            id:2,
            name:"Cli7 - Bekaert Industries Private Limited - Bekaert"
        }
    ]
})

export const mutations = {
    setDeployedAt(state,list){
        state.deployedAt = list
    }
}

export const getters = {
    getDeployedAt(state){
        return state.deployedAt
    }
}

export const actions = {
	
}
