import Vue from 'vue'
export const state = () => ({
  formGenerated: [
    // {
    //   "id": 0,
    //   "componentType": "FormHeaderComponent",
    //   "editable": false,
    //   "deleted": false,
    //   "children": [],
    //   "text":"heading",
    //   "type":"h1"
    // }, 
    // {
    //   "id": 1,
    //   "componentType": "FormTabComponent",
    //   "editable": true,
    //   "deleted": false,
    //   "name":"tab1",
    //   "class":"layout-1",
    //   "children": [
    //     {
    //     "id": 0,
    //     "componentType": "FormHeaderComponent",
    //     "editable": false,
    //     "deleted": true,
    //     "text":"heading",
    //     "type":"h1"
    //   }, 
    //   {
    //     "id": 1,
    //     "componentType": "FormInputComponent",
    //     "editable": false,
    //     "deleted": false,
    //     "placeholder":"Enter your name",
    //     "type":"text",
    //     "label":"Enter your name"
    //   }]
    // }, 
    // {
    //   "id": 2,
    //   "componentType": "FormLayout",
    //   "class":"layout-2",
    //   "editable": true,
    //   "deleted": false,
    //   "children": 
    //   [
    // {
    //     "id": 0,
    //     "componentType": "FormHeaderComponent",
    //     "editable": false,
    //     "deleted": true
    //   }, 
    //   {
    //     "id": 1,
    //     "componentType": "FormInputComponent",
    //     "editable": false,
    //     "deleted": false,
    //     "placeholder":"Enter your name",
    //     "type":"textarea",
    //     "label":"Enter your name"
    //   }, 
    //   {
    //     "id": 2,
    //     "componentType": "FormInputComponent",
    //     "editable": false,
    //     "deleted": false,
    //     "placeholder":"Enter your number",
    //     "type":"number",
    //     "label":"Enter your number"
    //   },
    //   {
    //     "id": 3,
    //     "componentType": "FormRadioComponent",
    //     "editable": false,
    //     "deleted": false,
    //     "placeholder":"Select Option",
    //     "type":"radio",
    //     "label":"Select a Option",
    //     "options":
    //     [
    //         {
    //             id:"Option 1",
    //             value:"Option 1"
    //         }
    //     ]
    //   },
    //   {
    //     "id": 4,
    //     "componentType": "FormSelectComponent",
    //     "editable": false,
    //     "deleted": false,
    //     "placeholder":"Select Option",
    //     "type":"radio",
    //     "label":"Select a Option",
    //     "options":
    //     [
    //         {
    //             id:"Option 1",
    //             value:"Option 1"
    //         }
    //     ]
    //   },
    //   {
    //     "id": 5,
    //     "componentType": "FormCheckboxComponent",
    //     "editable": false,
    //     "deleted": false,
    //     "placeholder":"Select Option",
    //     "type":"radio",
    //     "label":"Select a Option",
    //     "options":
    //     [
    //         {
    //             id:"Option 1",
    //             value:"Option 1"
    //         }
    //     ]
    //   }
    // ]
    // }
  ],
  type: 'Edit',
})

export const mutations = {
  setFormGenerated(state, list) {
    state.formGenerated.push(list)
  },
  setSelectedIndex(state, index) {
    state.selectedIndex = index
  },
  setEditStatus(state, value) {
    if (value.childIndex != null) {
      state.formGenerated[value.parentIndex].children[value.childIndex].editable = value.editStatus
    } else {
      return state.formGenerated[value.parentIndex].editable = value.editStatus
    }
  },
  setProperties(state,value){
    console.log('properties ')
    if (value.childIndex != null) {
      Object.assign(state.formGenerated[value.parentIndex].children[value.childIndex], value)
    } else {
      Object.assign(state.formGenerated[value.parentIndex],value)
    }
  },
  // setTabChild(state,value){
  //   state.formGenerated[value.parentIndex].children = value
  // },
  removeFormElement(state, value) {
    if (value.childIndex != null) {
      // state.formGenerated[value.parentIndex].children[value.childIndex].deleted = true
      state.formGenerated[value.parentIndex].children.splice(value.childIndex,1)
    } else {
      // state.formGenerated[value.parentIndex].deleted = true
      state.formGenerated.splice(value.parentIndex,1)
    }
  },
  updateFormOrder(state, list) {
    state.formGenerated = []
    list.forEach((item) => {
      state.formGenerated.push(item)
    })
  },
  updateTheTabComponent(state, list) {
    state.formGenerated[list.index].children.push(list.item)
  },
  setFormType(state,value){
      state.type=value
  }
}

export const getters = {
  getFormGenerated(state) {
    return state.formGenerated
  },
  getselectedIndex(state) {
    return state.selectedIndex
  },
  getFormEditState: (state) => (value) => {
    if (value.childIndex != null) {
      return state.formGenerated[value.parentIndex].children[value.childIndex].editable
    } else {
      return state.formGenerated[value.parentIndex].editable
    }
  },
  getFormSection(state){
    console.log('get form',state.formGenerated.length)
    let arr = []
    if(state.formGenerated.length==1){
      arr.push({
        value:'Section 1',
        label:'Section 1'
      })
    }else{
      state.formGenerated.forEach((item)=>{
        if(item.componentType=="FormTabComponent"){
          if(item.name != undefined){

            arr.push({
              value:item.name,
              label:item.name
            })
          }else{
            arr.push({
              value:'Section 1',
              label:'Section 1'
            })
          }
        }
      })
      
    }
    console.log(arr)
    return arr
  },
  getFormType(state){
      return state.type
  },
  getFormProperties:(state) => (value) =>{
        if (value.childIndex != null) {
        return state.formGenerated[value.parentIndex].children[value.childIndex]
      } else {
        return state.formGenerated[value.parentIndex]
      } 
  },
  getDependentOnField(state){
    let dependentOnFields=[{
      value:"None",
      label:"None"
    }]
    state.formGenerated.forEach((item)=>{
        if(item.children.length==0){
          if(item.name!=undefined){
            dependentOnFields.push({
              value:item.name,
              label:item.name
            })
          }
        }else{
          item.children.forEach((child)=>{
              if(child.name!=undefined){
                  dependentOnFields.push({
                    value:child.name,
                    label:child.name
                  })
              }
          })
        }
    })
    // if(childIndex)
    return dependentOnFields
  }
}

export const actions = {
  addToFormGenerated({
    commit
  }, value) {
    commit('setFormGenerated', value)
  },
  editToFormGenerated({
    commit,
    state
  }, value) {

  },
  removeFormGenerated({
    commit,
    state
  }) {

  },
  swapItems({
    commit,
    state
  }, obj) {

    var fromArray = state.formGenerated[obj.start]
    var toArray = state.formGenerated[obj.end]
    var tempArray = state.formGenerated
    tempArray[obj.start] = toArray
    tempArray[obj.end] = fromArray
    console.log(tempArray)
    commit('updateFormOrder', tempArray)
  }

}
