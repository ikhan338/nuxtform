/* eslint-disable no-console */
import Vue from 'vue'
export const state = () => ({
    step:"1",
    EmployeeInformation:"formData" in localStorage ?  localStorage.getItem('formData'):{}
})

export const mutations = {
    setStep(state,list){
        state.step = list
    },
    setEmployeeInformation(state,list){
        localStorage.setItem('formData',JSON.stringify(list))
        state.EmployeeInformation = list
    }
}

export const getters = {
    getStep(state){
        return state.step
    },
    getEmployeeInformation(state){
        return "formData" in localStorage ?  localStorage.getItem('formData'):{}
    }
}

export const actions = {
	incrementStep({commit,state}){
        commit('setStep',String(Number(state.step) + 1))
    },
    decrementStep({commit,state}){
        commit('setStep',String(Number(state.step) - 1))
    },
    pushEmployeeInformation({ commit,state },payload){
        const tempEmpObj = Object.assign(state.EmployeeInformation,payload)
        commit('setEmployeeInformation',tempEmpObj)
    }
}
