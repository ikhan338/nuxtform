/* eslint-disable no-console */
import Vue from 'vue'
export const state = () => ({
  formType: 'create',
  employeeInfo: {
    //   "basicInformation": {
    //     "gender": "Male",
    //     "agencyZone": "West",
    //     "AgencyBranch": "Dharwad",
    //     "deployedAt": "Cli9 - TOPLINE BUSINESS SERVICES PVT LTD - BENGALORE",
    //     "designation": "Chennai",
    //     "employeeid": "12212",
    //     "employeename": "imran",
    //     "employeemail": "test@three38inc.edu",
    //     "phoneno": "9449221270",
    //     "password": "ST-123457",
    //     "dob": "2022-04-26",
    //     "dateOfJoining": "2022-04-19"
    //   },
    // "additionalInformation": {
    //   "secondaryphoneno": "9449221270",
    //   "pemail": "james@three38inc.com",
    //   "emergencyContactno": "9449221270"
    // },
    // "familyInformation": [{
    //   "name": "Mohamed Imran Khan S",
    //   "address": "pooli puthu Street/melaplayam",
    //   "relation": "mom"
    // }]
  }
})

export const mutations = {
  setEmployeeInfo(state, list) {
    localStorage.setItem('formData',JSON.stringify(list))
    state.employeeInfo = list
  },
  setFormType(state, value) {
    state.formType = value
  }
}

export const getters = {
  getEmployeeInfo(state) {
    return state.employeeInfo
  }
}

export const actions = {

}
